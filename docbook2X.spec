Name:            docbook2X
Version:         0.8.8
Release:         32
Summary:         Convert docbook into man and Texinfo
License:         MIT
URL:             http://docbook2x.sourceforge.net/
Source0:         http://downloads.sourceforge.net/docbook2x/docbook2X-%{version}.tar.gz
BuildRequires:   perl-interpreter perl-generators libxslt openjade texinfo opensp
BuildRequires:   perl(XML::SAX::ParserFactory)
Requires:        libxslt openjade texinfo opensp perl(Exporter) perl(IO::File)
Requires:        perl(Text::Wrap) perl(vars) perl(XML::SAX::ParserFactory)
Requires:        perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires(post):  texinfo
Requires(preun): texinfo

%description
docbook2X is a software package that converts DocBook documents into the
traditional Unix man page format and the GNU Texinfo format.

%package         help
Summary:         Documention for docboox2X package

%description     help
Documention for docbook2X package.

%prep
%autosetup -p1

%build
%configure --program-transform-name='s/docbook2/db2x_docbook2/'
%make_build
rm -rf __dist_html
install -d __dist_html/html
install -p doc/*.html __dist_html/html

%install
%make_install

%post
/sbin/install-info %{_infodir}/docbook2X.info %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
/sbin/install-info --delete %{_infodir}/docbook2X.info %{_infodir}/dir || :
fi

%files
%doc COPYING AUTHORS
%{_bindir}/*
%dir %{_datadir}/docbook2X
%dir %{_datadir}/docbook2X/charmaps
%dir %{_datadir}/docbook2X/dtd
%dir %{_datadir}/docbook2X/xslt
%{_datadir}/docbook2X/*
%exclude %{_datadir}/doc/
%exclude %{_infodir}/dir

%files help
%doc README THANKS __dist_html/html/
%{_mandir}/man1/*.1*
%{_infodir}/docbook2*

%changelog
* Fri Nov 29 2019 zhujunhao <zhujunhao5@huawei.com> - 0.8.8-32
- Package init
